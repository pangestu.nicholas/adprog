package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

public class FamiliarSealSpell extends FamiliarSpell {
    // TODO: Complete Me

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
