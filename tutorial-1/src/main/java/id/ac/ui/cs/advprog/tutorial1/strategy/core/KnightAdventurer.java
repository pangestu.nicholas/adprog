package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    AttackBehavior attack;
    DefenseBehavior defense;
    public class AgileAdventurer extends Adventurer {
        public AgileAdventurer(String name){
            this.attack=new AttackWithSword();
            this.defense=new DefendWithArmor();
        }
        public String getAlias(){
            return "Knight";
        }
        //ToDo: Complete me
        public String getAttackBehavior(){
            return attack;
        }

        public String getDefenseBehavior(){
            return defense;
        }
    }

}
