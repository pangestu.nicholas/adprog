package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    AttackBehavior attack;
    DefenseBehavior defense;
    public class AgileAdventurer extends Adventurer {
        public AgileAdventurer(String name){
            this.attack=new AttackWithMagic();
            this.defense=new DefendWithShield();
        }
        public String getAlias(){
            return "Mystic";
        }
        //ToDo: Complete me
        public String getAttackBehavior(){
            return attack;
        }

        public String getDefenseBehavior(){
            return defense;
        }
    }
        //ToDo: Complete me
}
