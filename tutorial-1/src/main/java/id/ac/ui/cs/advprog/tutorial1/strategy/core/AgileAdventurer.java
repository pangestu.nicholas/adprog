package id.ac.ui.cs.advprog.tutorial1.strategy.core;


public class AgileAdventurer extends Adventurer {
    AttackBehavior attack;
    DefenseBehavior defense;

    public AgileAdventurer(String name){
        this.attack=new AttackWithGun();
        this.defense=new DefendWithBarrier();
    }
    public String getAlias(){
        return "Agile";
    }
        //ToDo: Complete me
    public String getAttackBehavior(){
        return attack;
    }

    public String getDefenseBehavior(){
        return defense;
    }
}
